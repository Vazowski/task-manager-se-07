package ru.iteco.taskmanager;

import org.jetbrains.annotations.NotNull;

import ru.iteco.taskmanager.bootstrap.Bootstrap;
import ru.iteco.taskmanager.command.project.ProjectFindAllCommand;
import ru.iteco.taskmanager.command.project.ProjectFindCommand;
import ru.iteco.taskmanager.command.project.ProjectMergeCommand;
import ru.iteco.taskmanager.command.project.ProjectPersistCommand;
import ru.iteco.taskmanager.command.project.ProjectRemoveAllCommand;
import ru.iteco.taskmanager.command.project.ProjectRemoveCommand;
import ru.iteco.taskmanager.command.system.AboutCommand;
import ru.iteco.taskmanager.command.system.ExitCommand;
import ru.iteco.taskmanager.command.system.HelpCommand;
import ru.iteco.taskmanager.command.task.TaskFindAllCommand;
import ru.iteco.taskmanager.command.task.TaskFindCommand;
import ru.iteco.taskmanager.command.task.TaskMergeCommand;
import ru.iteco.taskmanager.command.task.TaskPersistCommand;
import ru.iteco.taskmanager.command.task.TaskRemoveAllCommand;
import ru.iteco.taskmanager.command.task.TaskRemoveCommand;
import ru.iteco.taskmanager.command.user.UserCreateCommand;
import ru.iteco.taskmanager.command.user.UserLoginCommand;
import ru.iteco.taskmanager.command.user.UserLogoutCommand;
import ru.iteco.taskmanager.command.user.UserShowInfoCommand;
import ru.iteco.taskmanager.command.user.UserUpdateInfoCommand;
import ru.iteco.taskmanager.command.user.UserUpdatePasswordCommand;

public class App
{
	
	private static final Class[] CLASSES = {
			AboutCommand.class, ExitCommand.class, HelpCommand.class,
			
			UserCreateCommand.class, UserLoginCommand.class, UserLogoutCommand.class,
			UserShowInfoCommand.class, UserUpdateInfoCommand.class, UserUpdatePasswordCommand.class,
			
			ProjectFindCommand.class, ProjectFindAllCommand.class, ProjectMergeCommand.class,
			ProjectPersistCommand.class, ProjectRemoveCommand.class, ProjectRemoveAllCommand.class,
			
			TaskFindCommand.class, TaskFindAllCommand.class, TaskMergeCommand.class,
			TaskPersistCommand.class, TaskRemoveCommand.class, TaskRemoveAllCommand.class,
	};
	
    public static void main(final String[] args )
    {
    	@NotNull
    	final Bootstrap bootstrap = new Bootstrap();
    	bootstrap.init(CLASSES);
    }
}
