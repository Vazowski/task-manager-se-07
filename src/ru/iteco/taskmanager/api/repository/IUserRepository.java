package ru.iteco.taskmanager.api.repository;

import ru.iteco.taskmanager.enumerate.RoleType;
import ru.iteco.taskmanager.entity.User;

public interface IUserRepository {

	void merge(String login, String passwordHash, RoleType roleType, String uuid);
	String getPasswordHash(String uuid);
	User get(String login);
}
