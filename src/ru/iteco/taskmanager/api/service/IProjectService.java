package ru.iteco.taskmanager.api.service;

import java.util.List;

import ru.iteco.taskmanager.entity.Project;

public interface IProjectService {

	void merge(final String name, final String description, final String uuid, final String ownerId);
	void remove(final String name);
	void removeAll();
	Project getByUuid(String uuid);
	Project getByName(String name);
	List<Project> getAll();
}
