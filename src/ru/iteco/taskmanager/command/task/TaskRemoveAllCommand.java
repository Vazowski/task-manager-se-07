package ru.iteco.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.entity.Project;

public final class TaskRemoveAllCommand extends AbstractCommand {

	private String inputProjectName, projectUuid;
	
	@Override
	public String command() {
		return "task-remove-all";
	}

	@Override
	public String description() {
		return "  -  remove all task in project";
	}

	@Override
	public void execute() throws Exception {
		System.out.print("Name of project: ");
		inputProjectName = scanner.nextLine();
		@NotNull
		final String currentUser = serviceLocator.getUserService().getCurrent();
		@NotNull 
		final String userUuid = serviceLocator.getUserService().get(currentUser).getUuid();
		@Nullable 
		Project tempProject = serviceLocator.getProjectService().getByName(inputProjectName, userUuid);
		if (tempProject == null) {
			System.out.println("Project doesn't exist");
			return;
		} 
		projectUuid = tempProject.getUuid();
		serviceLocator.getTaskService().removeAllByProjectUuid(projectUuid);
		System.out.println("Done");
	}
}
