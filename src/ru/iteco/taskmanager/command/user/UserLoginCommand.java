package ru.iteco.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;

import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.entity.User;

public class UserLoginCommand extends AbstractCommand {

	private String login, password;
	private String hashPass;
	
	@Override
	public String command() {
		return "login";
	}

	@Override
	public String description() {
		return "  -  login user";
	}

	@Override
	public void execute() throws Exception {
		while(true) {
			System.out.print("Login: ");
			login = scanner.nextLine();
			System.out.print("Password: ");
			password = scanner.nextLine();
			hashPass = serviceLocator.getUserService().getPassword(password);
			@NotNull 
			final User tempUser = serviceLocator.getUserService().findByLogin(login);
			if (!tempUser.getPasswordHash().equals(hashPass)) {
				System.out.println("Username or password was incorrect. Try again");
				continue;
			}
			System.out.println("Success");
			if (password.equals("0000")) {
				System.out.print("Please type your new password: ");
				password = scanner.nextLine();
				serviceLocator.getUserService().merge(login, serviceLocator.getUserService().getPassword(password));
				System.out.println("Done");
			}
			serviceLocator.getUserService().setCurrent(login);
			break;
		}
	}
}
