package ru.iteco.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;

import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.entity.User;

public final class UserShowInfoCommand extends AbstractCommand {

	@Override
	public String command() {
		return "user-show";
	}

	@Override
	public String description() {
		return "  -  show user info";
	}

	@Override
	public void execute() throws Exception {
		@NotNull final User tempUser = serviceLocator.getUserService().findByLogin(serviceLocator.getUserService().getCurrent());
		System.out.println("Current login: " + tempUser.getLogin());
		System.out.println("Current role: " + tempUser.getRoleType());
	}
}
