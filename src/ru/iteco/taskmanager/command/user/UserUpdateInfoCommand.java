package ru.iteco.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;

import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.entity.User;

public final class UserUpdateInfoCommand extends AbstractCommand {

	@Override
	public String command() {
		return "user-update";
	}

	@Override
	public String description() {
		return "  -  update user information";
	}

	@Override
	public void execute() throws Exception {
		System.out.print("New login: ");
		@NotNull 
		final String name = scanner.nextLine();
		@NotNull 
		final User tempUser = serviceLocator.getUserService().findByLogin(serviceLocator.getUserService().getCurrent());
		serviceLocator.getUserService().merge(name, tempUser.getPasswordHash());
		serviceLocator.getUserService().setCurrent(name);
		System.out.println("Done");
	}
}
