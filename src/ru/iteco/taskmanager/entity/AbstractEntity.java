package ru.iteco.taskmanager.entity;

import java.text.SimpleDateFormat;

import org.jetbrains.annotations.NotNull;

import lombok.Getter;
import lombok.Setter;

public class AbstractEntity {

	protected SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
	@NotNull
	@Getter 
	@Setter
	protected String uuid;
}
