package ru.iteco.taskmanager.repository;

import java.util.ArrayList;
import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.repository.IProjectRepository;
import ru.iteco.taskmanager.entity.Project;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository{

	public ProjectRepository() {
		
	}
	
	public void merge(@NotNull final String name, @NotNull final String description, @NotNull final String uuid, @NotNull final String ownerId) {
		map.put(uuid, new Project(name, description, uuid, ownerId));
	}
	
	@Nullable
	public Project findByName(@NotNull final String name) {
		for (final Project project : map.values()) {
			if (project.getName().equals(name))
				return project;
		}
		return null;
	}
	
	@Nullable
	public Project findByName(@NotNull final String name, @NotNull final String ownerId) {
		for (final Project project : map.values()) {
			if (project.getName().equals(name) && project.getOwnerId().equals(ownerId))
				return project;
		}
		return null;
	}
	
	@Nullable
	public Project findByUuid(@NotNull final String uuid, @NotNull final String ownerId) {
		for (final Project project : map.values()) {
			if (project.getUuid().equals(uuid) && project.getOwnerId().equals(ownerId))
				return project;
		}
		return null;
	}
	
	@Nullable
	public List<Project> findAll(@NotNull final String ownerId) {
		@Nullable
		List<Project> resultList = new ArrayList<Project>();
		for (Project project : map.values()) {
			if (project.getOwnerId().equals(ownerId)) {
				resultList.add(project);
			}
		}
		return resultList;
	}
	
	public void remove(@NotNull final String uuid) {
		map.remove(uuid);
	}
	
	public void remove(@NotNull final String uuid, @NotNull final String ownerId) {
		if (map.get(uuid).getOwnerId().equals(ownerId))
			map.remove(uuid);
	}
}
