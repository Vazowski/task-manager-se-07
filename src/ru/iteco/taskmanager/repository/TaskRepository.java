package ru.iteco.taskmanager.repository;

import java.util.Iterator;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.repository.ITaskRepository;
import ru.iteco.taskmanager.entity.Task;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

	public TaskRepository() {

	}
	
	public void merge(@NotNull final String name, @NotNull final String description, @NotNull final String uuid, @NotNull final String projectUuid, @NotNull final String ownerId) {
		map.put(uuid, new Task(name, description, uuid, projectUuid, ownerId));
	}

	@Nullable
	public Task findByName(@NotNull final String name) {
		for (final Task task : map.values()) {
			if (task.getName().equals(name))
				return task;
		}
		return null;
	}
	
	@Nullable
	public Task findByName(@NotNull final String name, @NotNull final String ownerId) {
		for (final Task task : map.values()) {
			if (task.getName().equals(name) && task.getOwnerId().equals(ownerId))
				return task;
		}
		return null;
	}
	
	public void remove(@NotNull final String uuid) {
		map.remove(uuid);
	}
	
	public void remove(@NotNull final String uuid, @NotNull final String ownerId) {
		if (map.get(uuid).getOwnerId().equals(ownerId))
			map.remove(uuid);
	}
	
	public void removeByName(@NotNull final String name) {
		final Iterator<Task> iterator = map.values().iterator();
		while (iterator.hasNext()) {
			@Nullable 
			final Task task = iterator.next();
			if (task.getName().equals(name)) {
				iterator.remove();
				break;
			}
		}
	}
	
	public void removeAllByProjectUuid(@NotNull final String uuid) {
		final Iterator<Task> iterator = map.values().iterator();
		while (iterator.hasNext()) {
			@Nullable final Task task = iterator.next();
			if (task.getProjectUUID().equals(uuid)) {
				iterator.remove();
			}
		}
	}
}
