package ru.iteco.taskmanager.service;

import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.service.IProjectService;
import ru.iteco.taskmanager.entity.Project;
import ru.iteco.taskmanager.repository.ProjectRepository;
import ru.iteco.taskmanager.repository.TaskRepository;

public final class ProjectService extends AbstractService implements IProjectService {
	
	@NotNull
	private ProjectRepository projectRepository;
	@NotNull
	private TaskRepository taskRepository;
	
	public ProjectService(@NotNull final ProjectRepository projectRepository, @NotNull final TaskRepository taskRepository) {
		this.projectRepository = projectRepository;
		this.taskRepository = taskRepository;
	}
	
	public void merge(@NotNull final String name, @NotNull final String description, @NotNull final String uuid, @NotNull final String ownerId) {
		if (!name.equals(null) && !description.equals(null) &&  !uuid.equals(null) &&  !ownerId.equals(null))
			projectRepository.merge(name, description, uuid, ownerId);
	}

	public void remove(@NotNull final String uuid) {
		if (!uuid.equals(null)) {
			taskRepository.remove(uuid);
			projectRepository.remove(uuid);
		}
	}
	
	public void remove(@NotNull final String uuid, @NotNull final String ownerId) {
		if (!uuid.equals(null) && !uuid.equals(ownerId)) {
			taskRepository.remove(uuid, ownerId);
			projectRepository.remove(uuid, ownerId);
		}
	}
	
	public void removeAll() {
		taskRepository.clear();
		projectRepository.clear();
	}
	
	@Nullable
	public Project getByUuid(@NotNull final String uuid) {
		if (!uuid.equals(null)) {
			return projectRepository.findByUuid(uuid);
		}
		return null;
	}
	
	@Nullable
	public Project getByUuid(@NotNull final String uuid, @NotNull final String ownerId) {
		if (!uuid.equals(null) && !ownerId.equals(null)) {
			return projectRepository.findByUuid(uuid, ownerId);
		}
		return null;
	}
	
	@Nullable
	public Project getByName(@NotNull final String name) {
		if (!name.equals(null)) {
			return projectRepository.findByName(name);
		}
		return null;
	}
	
	@Nullable
	public Project getByName(@NotNull final String name, @NotNull final String ownerId) {
		if (!name.equals(null) && !ownerId.equals(null)) {
		return projectRepository.findByName(name, ownerId);
		}
		return null;
	}
	
	@Nullable
	public List<Project> getAll() {
		List<Project> list = projectRepository.findAll();
		if (list.size() > 0)
			return list;
		return null;
	}
	
	@Nullable
	public List<Project> getAll(@NotNull final String ownerId) {
		if (!ownerId.equals(null)) {
			final List<Project> list = projectRepository.findAll(ownerId);
			if (list.size() > 0)
				return list;
		}
		return null;
	}
}
